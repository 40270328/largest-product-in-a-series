﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;


namespace Largest_Product
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            /**
             * Setting the file path as a string to use later when converting the file to a CHAR ARRAY
             */
            //String text = System.IO.File.ReadAllText(@"H:\\Visual Studio 2013\\Projects\\Problem 8\\Largest Product\\test.txt");
            String text = System.IO.File.ReadAllText(@"F:\\Visual Studio 2013\\Projects\\Problem 8\\Largest Product\\test.txt");
            
            /*
             * Integers 'count' and 'pointer' used for the FOR LOOPS. 
             * Integer numberFromFile used to convert the CHAR to INT when getting the numeric value.
             */
            int numberFomFile = 0, pointer = 0, count = 0;

            /*
             * LONG 'sum' used to store the value of the 13 multiplied numbers.
             * LONG 'answer' used to store the highest value.
             * 
             * EDIT: LONG needed to be used as INT could not hold the highest value.
             */
            long sum = 0, answer = 0;
            var numbers = text.ToCharArray();

            /*
             * INT ARRAY used to store the individual numbers that have been read in from file and converted.
             */ 
            int[] digits = new int[numbers.Length];

            /*
             * LONG ARRAY used to store the values of the 13 multiplied numbers.
             */
            long[] product = new long[numbers.Length+1];

            /*
             * read in numbers from file and convert them to INT.
             * FOR LOOP used to store all numbers in the 'digits' ARRAY.
             */
            for(count = 0; count<numbers.Length; count++)
            {
                numberFomFile = (int)Char.GetNumericValue(numbers[count]);
                digits[count] = numberFomFile;      
            }

            /*
             * FOR LOOP used to multiply first 13 numbers.
             * 
             * First FOR LOOP also increments the Second FOR LOOP, in order to shuffle the section of 13
             * numbers along, in order to get next section of 13 numbers.
             */ 
            for (pointer = 0; pointer < digits.Length-12; pointer++)
            {
                /*
                 * Resets sum to 0 in order to avoid overlap of calculations.
                 */
                sum = 0;

                for (count = 0 + pointer; count < 13+ pointer; count++)
                {
                    if(count == 0 || sum == 0)
                    {
                        sum = digits[count];
                    }
                    else if (count < digits.Length)
                    {
                        sum = sum * digits[count];
                    }
                    else
                    {
                        //Do nothing.
                    } 
                }
                /*
                 * Store value of 13 multiplied numbers (The product) in the 'product' ARRAY
                 */
                product[pointer] = sum;

                /*
                 * Store the highest value found after multiplying in 'answer'
                 */
                if(sum > answer)
                {
                    answer = sum;
                }
            }
            /*
             * Print the highest value found to the label on the main window.
             */
            lblAnswer.Content = "Answer = " + answer;
        }
    }
}
